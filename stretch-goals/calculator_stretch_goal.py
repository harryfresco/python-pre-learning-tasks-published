def calculator(a, b, operator):
    binaryList = [128,64,32,16,8,4,2,1]
    
    binary = ''
    
    if operator == "+":
        answer = a + b
    elif operator == "-":
        answer = a - b
    elif operator == "*":
        answer = a * b
    elif operator == "/":
        answer = a / b
        answer = int(answer)

    for i in range(len(binaryList)):
        if answer - binaryList[i] >= 0:
            answer = answer - binaryList[i]
            binary += '1'
        else:
            binary += '0'

    return binary

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
