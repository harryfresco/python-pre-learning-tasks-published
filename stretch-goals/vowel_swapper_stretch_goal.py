def vowel_swapper(string):
    resultsA = []
    resultsE = []
    resultsI = []
    resultsO = []
    resultsU = []
    changed = ''

    ## Go through string and get indices of a, e, i
    for i in range(0, len(string)):
        if string[i] == "a" or string[i] == "A":
            resultsA.append(i)
        elif string[i] == "e" or string[i] == "E":
            resultsE.append(i)
        elif string[i] == "i" or string[i] == "I":
            resultsI.append(i)
    
        else:
            pass
        
    changed = string

    ## Change A E I
    if len(resultsA) > 1:
        changed = changed[:resultsA[1]] + "4" + changed[resultsA[1]+1:]
        
    if len(resultsE) > 1:
        changed = changed[:resultsE[1]] + "3" + changed[resultsE[1]+1:]


    if len(resultsI) > 1:
        changed = changed[:resultsI[1]] + "!" + changed[resultsI[1]+1:]


    ## Loop through changed string and get indices for O
    for j in range(0, len(changed)):
        if changed[j] == "o" or changed[j] == "O":
            resultsO.append(j)

    ## Change O
    if len(resultsO) > 1:
        if changed[int(resultsO[1])] == "o":
            changed = changed[:resultsO[1]] + "ooo" + changed[resultsO[1]+1:]

        
        elif changed[int(resultsO[1])] == "O":
            changed = changed[:resultsO[1]] + "000" + changed[resultsO[1]+1:]

    ## Loop through changed string and get indices for U
    for k in range(0, len(changed)):     
        if changed[k] == "u" or changed[k] == "U":
            resultsU.append(k)
    ## Change U
    if len(resultsU) > 1:
        changed = changed[:resultsU[1]] + "|_|" + changed[resultsU[1]+1:]

    
    return changed

    resultsA.clear()
    resultsE.clear()
    resultsI.clear()
    resultsO.clear()
    resultsU.clear()
    
    
print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
