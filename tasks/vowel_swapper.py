def vowel_swapper(string):
    changed = string.replace("a", "4")
    changed = changed.replace("A", "4")
    changed = changed.replace("e", "3")
    changed = changed.replace("E", "3")
    changed = changed.replace("i", "!")
    changed = changed.replace("I", "!")
    changed = changed.replace("o", "ooo")
    changed = changed.replace("O", "000")
    changed = changed.replace("u", "|_|")
    changed = changed.replace("U", "|_|")    
    
    return changed
                          

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
