def factors(number):
    factorList = []
    
    for i in range(1, number):
        if number % i == 0 and i != 1 and i != number:
            factorList.append(i)

    return factorList
        

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
